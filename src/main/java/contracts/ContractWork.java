package contracts;

public class ContractWork extends Payment {

	private double revenueCost;
	
	public ContractWork(String year, double paymentAmount, double revenueCost) {
		super(year, paymentAmount);
		
	}

	public double getRevenueCost() {
		return revenueCost;
	}

	public void setRevenueCost(double revenueCost) {
		this.revenueCost = revenueCost;
	}
	
}
