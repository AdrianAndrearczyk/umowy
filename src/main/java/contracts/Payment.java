package contracts;

public class Payment {
	private String year;
	private double paymentAmount;
	private String paymentType;
	
	public Payment(String year, double paymentAmount) {
		super();
		this.year = year;
		this.paymentAmount = paymentAmount;
	}
	
	public Payment(String paymentType)
	{
		this.paymentType = paymentType;
	}

	public String getYear() {
		return year;
	}

	public void setYear(String year) {
		this.year = year;
	}

	public double getPaymentAmount() {
		return paymentAmount;
	}

	public void setPaymentAmount(double paymentAmount) {
		this.paymentAmount = paymentAmount;
	}

	public String getPaymentType() {
		return paymentType;
	}

	public void setPaymentType(String paymentType) {
		this.paymentType = paymentType;
	}
	
	
}
