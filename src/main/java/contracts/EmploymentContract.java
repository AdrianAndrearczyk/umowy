package contracts;

public class EmploymentContract {
	
    private Double disabilityContribution;
    private Double pensionContribution;
    private Double sicknessContribution;
    private Double healthCareContribution;
    
	public Double getDisabilityContribution() {
		return disabilityContribution;
	}
	public void setDisabilityContribution(Double disabilityContribution) {
		this.disabilityContribution = disabilityContribution;
	}
	public Double getPensionContribution() {
		return pensionContribution;
	}
	public void setPensionContribution(Double pensionContribution) {
		this.pensionContribution = pensionContribution;
	}
	public Double getSicknessContribution() {
		return sicknessContribution;
	}
	public void setSicknessContribution(Double sicknessContribution) {
		this.sicknessContribution = sicknessContribution;
	}
	public Double getHealthCareContribution() {
		return healthCareContribution;
	}
	public void setHealthCareContribution(Double healthCareContribution) {
		this.healthCareContribution = healthCareContribution;
	}
    
    
}
