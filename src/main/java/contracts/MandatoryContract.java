package contracts;

public class MandatoryContract extends ContractWork{

    private String disabilityContribution;
    private String pensionContribution;
    private String sicknessContribution;
    private String healthCareContribution;
	
	public MandatoryContract(String year, double paymentAmount, double revenueCost, String disabilityContribution,
			String pensionContribution, String sicknessContribution, String healthCareContribution) 
	{
		super(year, paymentAmount, revenueCost);
	}

	public String getDisabilityContribution() {
		return disabilityContribution;
	}

	public void setDisabilityContribution(String disabilityContribution) {
		this.disabilityContribution = disabilityContribution;
	}

	public String getPensionContribution() {
		return pensionContribution;
	}

	public void setPensionContribution(String pensionContribution) {
		this.pensionContribution = pensionContribution;
	}

	public String getSicknessContribution() {
		return sicknessContribution;
	}

	public void setSicknessContribution(String sicknessContribution) {
		this.sicknessContribution = sicknessContribution;
	}

	public String getHealthCareContribution() {
		return healthCareContribution;
	}

	public void setHealthCareContribution(String healthCareContribution) {
		this.healthCareContribution = healthCareContribution;
	}
	
}
