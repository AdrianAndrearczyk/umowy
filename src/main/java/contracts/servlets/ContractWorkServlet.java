package contracts.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import contracts.ContractWork;

@WebServlet("/contractWork")
public class ContractWorkServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		java.text.DecimalFormat df=new java.text.DecimalFormat("0.00"); 
		
		String year = (request.getParameter("year"));
		String paymentA = request.getParameter("payment_amount");
		Double revenueCost = Double.parseDouble(request.getParameter("revenue_cost"));
		
	    Double paymentAmount = Double.parseDouble(paymentA);
		if(revenueCost == 20)
		{
			revenueCost = paymentAmount * 0.2;
		}
		if(revenueCost == 50)
		{
			revenueCost = paymentAmount * 0.5;
		}
		if(revenueCost==null || revenueCost.equals(""))
		{
			response.sendRedirect("/contractWorkForm.jsp");
		}
	    if(year==null || year.equals("") || year.isEmpty())
	    {
	    	response.sendRedirect("/employmentContractForm.jsp");
	    }
	    if(paymentA==null || paymentA.equals("") || paymentA.isEmpty())
		{
	    	response.sendRedirect("/employmentContractForm.jsp");;
		}
	  

		Double pitAdvance = paymentAmount * 0.144;
		Double taxBase = paymentAmount - revenueCost;
		 Double netPaymentAmount = paymentAmount 
		    		- pitAdvance
		    		- revenueCost;
		 
	    response.setContentType("text/html");
	    response.getWriter().println("<html>");
		response.getWriter().println("<head>");
	    response.getWriter().println("<style type='text/css'>");
	    response.getWriter().println(".tg  {border-collapse:collapse;border-spacing:0;}");
	    response.getWriter().println(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}");
	    response.getWriter().println(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}");
	    response.getWriter().println(".tg .tg-baqh{text-align:center;vertical-align:top}");
	    response.getWriter().println(".tg .tg-yw4l{vertical-align:top}");
	    response.getWriter().println("</style>");
		response.getWriter().println("</head>");
		response.getWriter().println("<body>");	
		
		response.setContentType("text/html");
		response.getWriter().println("<h1>Umowa o dzielo na rok " + year + "</h1>");
		response.getWriter().println("<table class='tg'>");
	    response.getWriter().println("<tr>");
	    response.getWriter().println("<th rowspan='2'><b>Brutto</b></th>");
	    response.getWriter().println("<th rowspan='2'>Koszt<br>uzyskania<br>przychodu</th>");
	    response.getWriter().println("<th rowspan='2'>Podstawa<br>opodatkowania</th>");
	    response.getWriter().println("<th rowspan='2'>Zaliczka<br>na PIT</th>");
	    response.getWriter().println("<th rowspan='2'><b>Netto</b></th>");
	    response.getWriter().println("</tr>");
	    
	    response.getWriter().println("<tr>");
	    response.getWriter().println("</tr>");
	    response.getWriter().println("<tr>");
    	
	    response.getWriter().println("<td><b>");
	    response.getWriter().println(df.format(paymentAmount));
	    response.getWriter().println("</b></td>");
	    
	    response.getWriter().println("<td>");
	    response.getWriter().println(df.format(revenueCost));
	    response.getWriter().println("</td>");
	    
	    response.getWriter().println("<td>");
	    response.getWriter().println(df.format(taxBase));
	    response.getWriter().println("</td>");
	    
	    response.getWriter().println("<td>");
	    response.getWriter().println(df.format(pitAdvance));
	    response.getWriter().println("</td>");
	    
	    response.getWriter().println("<td><b>");
	    response.getWriter().println(df.format(netPaymentAmount));
	    response.getWriter().println("</b></td>");
	    
	    response.getWriter().println("</tr>");
	    response.getWriter().println("</table>");
	}
}
