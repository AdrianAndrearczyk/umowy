package contracts.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import contracts.EmploymentContract;
import contracts.Payment;


@WebServlet("/employmentContract")
public class EmploymentContractServlet extends HttpServlet{
	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
		
		java.text.DecimalFormat df=new java.text.DecimalFormat("0.00"); 
		
		
		
		String year = request.getParameter("year");
		String paymentA = request.getParameter("payment_amount");
	    if(year==null || year.equals("") || year.isEmpty())
	    {
	    	response.sendRedirect("/employmentContractForm.jsp");
	    }
	    
	    if(paymentA==null || paymentA.equals("") || paymentA.isEmpty())
		{
	    	response.sendRedirect("/employmentContractForm.jsp");;
		}
	  
	    Double paymentAmount = Double.parseDouble(paymentA);
	    	
	    
	
		
		
		
		Double disabilityContribution = paymentAmount * 0.098;
	    Double pensionContribution = paymentAmount * 0.015;
	    Double sicknessContribution = paymentAmount * 0.025;
	    Double healthCareContribution = paymentAmount * 0.078;
	    Double taxBase = paymentAmount * 0.8;
	    Double pitAdvance = paymentAmount * 0.053;
	    Double netPaymentAmount = paymentAmount 
	    		- disabilityContribution 
	    		- pensionContribution 
	    		- sicknessContribution 
	    		- healthCareContribution 
	    		- pitAdvance;
	    
	    String[] months = {"Styczen", "Luty", "Marzec", 
	    					"Kwiecien", "Maj", "Czerwiec",
	    					"Lipiec", "Sierpien", "Wrzesien",
	    					"Pazdziernik", "Listopad", "Grudzien"};
	    
	    Double[] sum = {paymentAmount, disabilityContribution, pensionContribution, 
	    		sicknessContribution, healthCareContribution, taxBase, pitAdvance, netPaymentAmount};
	   
	    

	    

	    
	    response.setContentType("text/html");
		response.getWriter().println("<head>");
	    response.getWriter().println("<style type='text/css'>");
	    response.getWriter().println(".tg  {border-collapse:collapse;border-spacing:0;}");
	    response.getWriter().println(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}");
	    response.getWriter().println(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}");
	    response.getWriter().println(".tg .tg-baqh{text-align:center;vertical-align:top}");
	    response.getWriter().println(".tg .tg-yw4l{vertical-align:top}");
	    response.getWriter().println("</style>");
		response.getWriter().println("</head>");
		response.getWriter().println("<body>");	
		
		response.getWriter().println("<h1>Umowa o prace na rok " + year + "</h1>");
	    response.getWriter().println("<table class='tg'>");
	    response.getWriter().println("<tr>");
	    response.getWriter().println("<th class='tg-031e' rowspan='2'></th>");
	    response.getWriter().println("<th class='tg-yw4l' rowspan='2'><b>Brutto</b></th>");
	    response.getWriter().println("<th class='tg-baqh' colspan='4'>Ubezpieczenie</th>");
	    response.getWriter().println("<th class='tg-baqh' rowspan='2'>Podstawa<br>Opodatkowania</th>");
	    response.getWriter().println("<th class='tg-baqh' rowspan='2'>Zaliczka<br>na PIT</th>");
	    response.getWriter().println("<th class='tg-yw4l' rowspan='2'><b>Netto</b></th>");
	    response.getWriter().println("</tr>");
	    response.getWriter().println("<tr>");
	    response.getWriter().println("<td class='tg-yw4l'>emerytalne</td>");
	    response.getWriter().println("<td class='tg-yw4l'>rentowe</td>");
	    response.getWriter().println("<td class='tg-yw4l'>chorobowe</td>");
	    response.getWriter().println("<td class='tg-yw4l'>zdrowotne</td>");
	    response.getWriter().println("</tr>");
	    
	    for (int i=0; i<12; i++)
	    {
	    	response.getWriter().println("<tr>");
	    	response.getWriter().println("<td class='tg-yw4l'>");
	    	response.getWriter().println("<b>");	
	    	response.getWriter().println(months[i]);
	    	response.getWriter().println("</b>");	
	    	response.getWriter().println("</td>");
	    
	    	response.getWriter().println("<td class='tg-yw4l'>");
	    	response.getWriter().println("<b>");
	    	response.getWriter().println(paymentAmount);
	    	response.getWriter().println("</b>");
	    	response.getWriter().println("</td>");

	    	response.getWriter().println("<td class='tg-yw4l'>");
	    	response.getWriter().println(df.format(disabilityContribution));
	    	response.getWriter().println("</td>");
	    	
	    	response.getWriter().println("<td class='tg-yw4l'>");
	    	response.getWriter().println(df.format(pensionContribution));
	    	response.getWriter().println("</td>");
	    	
	    	response.getWriter().println("<td class='tg-yw4l'>");
	    	response.getWriter().println(df.format(sicknessContribution));
	    	response.getWriter().println("</td>");
	    	
	    	response.getWriter().println("<td class='tg-yw4l'>");
	    	response.getWriter().println(df.format(healthCareContribution));
	    	response.getWriter().println("</td>");
	    	
	    	response.getWriter().println("<td class='tg-yw4l'>");
	    	response.getWriter().println(df.format(taxBase));
	    	response.getWriter().println("</td>");
	    	
	    	response.getWriter().println("<td class='tg-yw4l'>");
	    	response.getWriter().println(df.format(pitAdvance));
	    	response.getWriter().println("</td>");
	    	
	    	response.getWriter().println("<td class='tg-yw4l'>");
	    	response.getWriter().println("<b>");
	    	response.getWriter().println(df.format(netPaymentAmount));
	    	response.getWriter().println("</b>");
	    	response.getWriter().println("</td>");
	    	response.getWriter().println("</tr>");			
	    }
	    response.getWriter().println("<tr><td>");
    	response.getWriter().println("<b>");
	    response.getWriter().println("suma");
    	response.getWriter().println("</b>");
	    response.getWriter().println("</td>");
	    
	    for (int j=0; j<8; j++)
	    {
	    	response.getWriter().println("<td>");
	    	response.getWriter().println("<b>");
	    	response.getWriter().println(df.format(sum[j]*12));
	    	response.getWriter().println("</b>");
	    	response.getWriter().println("</td>");
	    }
	    response.getWriter().println("</tr>");
	    response.getWriter().println("</table>");
	    response.getWriter().println("</table>");
	    
		response.getWriter().println("</body>");
		response.getWriter().println("</html>");
		

	}
}
