package contracts.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import contracts.MandatoryContract;
import contracts.Payment;

@WebServlet("/mandatoryContract")
public class MandatoryContractServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		
	
		java.text.DecimalFormat df=new java.text.DecimalFormat("0.00"); 
		
		String year = (request.getParameter("year"));
		String paymentA = request.getParameter("payment_amount");
		
		Double revenueCost = Double.parseDouble(request.getParameter("revenue_cost"));
		Double paymentAmount = Double.parseDouble(paymentA);
		if(revenueCost == 20)
		{
			revenueCost = paymentAmount * 0.2;
		}
		if(revenueCost == 50)
		{
			revenueCost = paymentAmount * 0.5;
		}
		if(revenueCost==null || revenueCost.equals(""))
		{
			response.sendRedirect("/mandatoryContractForm.jsp");
		}
	    if(year==null || year.equals("") || year.isEmpty())
	    {
	    	response.sendRedirect("/mandatoryContractForm.jsp");
	    }
	    if(paymentA==null || paymentA.equals("") || paymentA.isEmpty())
		{
	    	response.sendRedirect("/mandatoryContractForm.jsp");;
		}
		String disabilityContributionOption = request.getParameter("disability_contribution");
		String pensionContributionOption = request.getParameter("pension_contribution");
		String sicknessContributionOption = request.getParameter("sickness_contribution");
		String healthCareContributionOption = request.getParameter("health_care_contribution");
		

		
		Double disabilityContribution = paymentAmount * 0.098;
	    Double pensionContribution = paymentAmount * 0.015;
	    Double sicknessContribution = paymentAmount * 0.025;
	    Double healthCareContribution = paymentAmount * 0.078;
	    Double taxBase = paymentAmount * 0.8;
	    Double pitAdvance = paymentAmount * 0.053;

	
	    if(revenueCost == 20)
		{
			revenueCost = paymentAmount * 0.2;
		}
		if(revenueCost == 50)
		{
			revenueCost = paymentAmount * 0.5;
		}
		if(revenueCost==null || revenueCost.equals(""))
		{
			response.sendRedirect("/mandatoryContractForm.jsp");
		}
	    
    	if (disabilityContributionOption.equals("no"))
    	{
    	disabilityContribution = 0.00;
    	}
    	if (pensionContributionOption.equals("no"))
    	{
    		pensionContribution = 0.00;
    	}
    	if (sicknessContributionOption.equals("no"))
    	{
    		sicknessContribution = 0.00;
    	}
    	if (healthCareContributionOption.equals("no"))
    	{
    		healthCareContribution = 0.00;
    	}
    	
	    Double netPaymentAmount = paymentAmount 
	    		- disabilityContribution 
	    		- pensionContribution 
	    		- sicknessContribution 
	    		- healthCareContribution 
	    		- pitAdvance
	    		- revenueCost;
	    
	    response.setContentType("text/html");
	    response.getWriter().println("<html>");
		response.getWriter().println("<head>");
	    response.getWriter().println("<style type='text/css'>");
	    response.getWriter().println(".tg  {border-collapse:collapse;border-spacing:0;}");
	    response.getWriter().println(".tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}");
	    response.getWriter().println(".tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;}");
	    response.getWriter().println(".tg .tg-baqh{text-align:center;vertical-align:top}");
	    response.getWriter().println(".tg .tg-yw4l{vertical-align:top}");
	    response.getWriter().println("</style>");
		response.getWriter().println("</head>");
		response.getWriter().println("<body>");	
		
		response.getWriter().println("<h1>Umowa o prace na rok " + year + "</h1>");
	    response.getWriter().println("<table class='tg'>");
	    response.getWriter().println("<tr>");
	    response.getWriter().println("<th class='tg-yw4l' rowspan='2'><b>Brutto</b></th>");
	    response.getWriter().println("<th class='tg-baqh' colspan='4'>Ubezpieczenie</th>");
	    response.getWriter().println("<th class='tg-baqh' rowspan='2'>Koszt<br>uzyskania<br>przychodu</th>");
	    response.getWriter().println("<th class='tg-baqh' rowspan='2'>Podstawa<br>Opodatkowania</th>");
	    response.getWriter().println("<th class='tg-baqh' rowspan='2'>Zaliczka<br>na PIT</th>");
	    response.getWriter().println("<th class='tg-yw4l' rowspan='2'><b>Netto</b></th>");
	    response.getWriter().println("</tr>");
	    response.getWriter().println("<tr>");
	    response.getWriter().println("<td class='tg-yw4l'>emerytalne</td>");
	    response.getWriter().println("<td class='tg-yw4l'>rentowe</td>");
	    response.getWriter().println("<td class='tg-yw4l'>chorobowe</td>");
	    response.getWriter().println("<td class='tg-yw4l'>zdrowotne</td>");
	    response.getWriter().println("</tr>");
	    
	    response.getWriter().println("<tr>");
    	response.getWriter().println("<td class='tg-yw4l'>");
    	response.getWriter().println("<b>");
    	response.getWriter().println(df.format(paymentAmount));
    	response.getWriter().println("</b>");
    	response.getWriter().println("</td>");
    	
    	response.getWriter().println("<td class='tg-yw4l'>");
    	response.getWriter().println(df.format(disabilityContribution));
    	response.getWriter().println("</td>");
    	
    	response.getWriter().println("<td class='tg-yw4l'>");
    	response.getWriter().println(df.format(pensionContribution));
    	response.getWriter().println("</td>");
    	
    	response.getWriter().println("<td class='tg-yw4l'>");
    	response.getWriter().println(df.format(sicknessContribution));
    	response.getWriter().println("</td>");
    	
    	response.getWriter().println("<td class='tg-yw4l'>");
    	response.getWriter().println(df.format(healthCareContribution));
    	response.getWriter().println("</td>");
    	
    	response.getWriter().println("<td class='tg-yw4l'>");
    	response.getWriter().println(df.format(revenueCost));
    	response.getWriter().println("</td>");
    	
    	response.getWriter().println("<td class='tg-yw4l'>");
    	response.getWriter().println(df.format(taxBase));
    	response.getWriter().println("</td>");
    	
    	response.getWriter().println("<td class='tg-yw4l'>");
    	response.getWriter().println(df.format(pitAdvance));
    	response.getWriter().println("</td>");
    	
    	response.getWriter().println("<td class='tg-yw4l'>");
    	response.getWriter().println("<b>");
    	response.getWriter().println(df.format(netPaymentAmount));
    	response.getWriter().println("</b>");
    	response.getWriter().println("</td>");
    	response.getWriter().println("</tr>");
	
	}
	
		
		
		
}
