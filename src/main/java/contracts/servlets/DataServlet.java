package contracts.servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import contracts.Payment;




@WebServlet("/data")
public class DataServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {

		String paymentType = request.getParameter("contract");
		Payment payment = new Payment(paymentType);
		
		
		if(paymentType==null || paymentType.equals("")|| paymentType.isEmpty())
		{
			response.sendRedirect("/");
		}
		else if(paymentType.equals("employment_contract"))
		{
			response.sendRedirect("/employmentContractForm.jsp");
		}
		else if(paymentType.equals("contract_work"))
		{
			response.sendRedirect("/contractWorkForm.jsp");
		}
		else if(paymentType.equals("mandatory_contract"))
		{
			response.sendRedirect("/mandatoryContractForm.jsp");
		}
	}
	
}
