<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Umowa zlecenie</title>
</head>
<body>
<h1>Umowa zlecenie</h1>
<form action="/mandatoryContract" method="POST">
<% GregorianCalendar cal = new GregorianCalendar(); %>
<table>
<tr><td>
Rok:
</td>
<td><input type="text" name="year" min=1901 value=<%out.print(cal.get(Calendar.YEAR));%> required><br>
</td></tr>
<tr><td>
Podaj kwote wynagrodzenia:
</td><td>
<input type="number" name="payment_amount" required><br>
</td></tr>

<tr><td>
Skladka rentowa:

</td>
<td>
Tak<input type="radio" name="disability_contribution" value="yes" required required> Nie <input type="radio" name="disability_contribution" value="no" required>
</td>
</tr>

<tr><td>
Skladka emerytalna:
</td>
<td>
Tak<input type="radio" name="pension_contribution" value="yes" required required> Nie <input type="radio" name="pension_contribution" value="no" required>
</td>
</tr>

<tr><td>
Skladka chorobowa:

</td>
<td>
Tak<input type="radio" name="sickness_contribution" value="yes" required required> Nie <input type="radio" name="sickness_contribution" value="no" required>
</td>
</tr>

<tr><td>
Skladka zdrowotna:
</td>
<td>
Tak<input type="radio" name="health_care_contribution" value="yes" required required> Nie <input type="radio" name="health_care_contribution" value="no" required>
</td>
</tr>

<tr><td>
Koszty uzyskania przychodu:
</td>
<td>
20%<input type="radio" name="revenue_cost" value=20 required> 50% <input type="radio" name="revenue_cost" value=50 required>
</td>
</tr>
</table>
<input type="submit">
</form>
</body>
</html>