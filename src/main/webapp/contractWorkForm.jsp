<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<%@page import="java.util.Calendar"%>
<%@page import="java.util.GregorianCalendar"%>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Umowa o dzielo</title>
</head>
<body>
<h1>Umowa o dzielo</h1>
<form action="/contractWork" method="POST">
<% GregorianCalendar cal = new GregorianCalendar(); %>
<table>
<tr><td>
Rok:
</td>
<td><input type="text" name="year" min=1901 value=<%out.print(cal.get(Calendar.YEAR));%> required><br>
</td></tr>
<tr><td>
Podaj kwote wynagrodzenia:
</td><td>
<input type="number" name="payment_amount" required><br>
</td></tr>
<tr><td>
Koszty uzyskania przychodu:
</td>
<td>
20%<input type="radio" name="revenue_cost" value=20 required> 50% <input type="radio" name="revenue_cost" value=50 required><td><br>
</tr>
</table>
<input type="submit">
</form>
</body>
</html>