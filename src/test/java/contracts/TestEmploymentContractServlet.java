package contracts;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import java.io.IOException;
import java.io.PrintWriter;

import contracts.servlets.EmploymentContractServlet;

public class TestEmploymentContractServlet extends Mockito{

	@Test
	public void servlet_should_not_go_further_if_payment_amount_number_is_null() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		EmploymentContractServlet servlet = new EmploymentContractServlet();
		PrintWriter writer = (PrintWriter) mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		
		when(request.getParameter("year")).thenReturn(null);
		when(request.getParameter("payment_amount")).thenReturn("2000");
		
		servlet.doPost(request, response);
		
		verify(response).sendRedirect("/employmentContractForm.jsp");
	}
	
	@Test
	public void servlet_should_not_go_further_if_payment_amount_number_is_empty() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		EmploymentContractServlet servlet = new EmploymentContractServlet();
		PrintWriter writer = (PrintWriter) mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		
		when(request.getParameter("year")).thenReturn("");
		when(request.getParameter("payment_amount")).thenReturn("2000");
		
		servlet.doPost(request, response);
		
		verify(response).sendRedirect("/employmentContractForm.jsp");

	}
}
