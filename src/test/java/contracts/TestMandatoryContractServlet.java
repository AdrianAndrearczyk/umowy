package contracts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import contracts.servlets.MandatoryContractServlet;

public class TestMandatoryContractServlet extends Mockito{

	@Test
	public void servlet_should_not_go_further_if_variables_are_null() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		MandatoryContractServlet servlet = new MandatoryContractServlet();
		PrintWriter writer = (PrintWriter) mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		
		when(request.getParameter("year")).thenReturn(null);
		when(request.getParameter("payment_amount")).thenReturn("2000");
		when(request.getParameter("revenue_cost")).thenReturn("20");
		when(request.getParameter("disability_contribution")).thenReturn("yes");
		when(request.getParameter("pension_contribution")).thenReturn("yes");
		when(request.getParameter("sickness_contribution")).thenReturn("no");
		when(request.getParameter("health_care_contribution")).thenReturn("yes");
		
		servlet.doPost(request, response);
		
		verify(response).sendRedirect("/mandatoryContractForm.jsp");
	}
	
	@Test
	public void servlet_should_not_go_further_if_variables_are_empty() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		MandatoryContractServlet servlet = new MandatoryContractServlet();
		PrintWriter writer = (PrintWriter) mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		
		when(request.getParameter("year")).thenReturn("");
		when(request.getParameter("payment_amount")).thenReturn("2000");
		when(request.getParameter("revenue_cost")).thenReturn("20");
		when(request.getParameter("disability_contribution")).thenReturn("no");
		when(request.getParameter("pension_contribution")).thenReturn("yes");
		when(request.getParameter("sickness_contribution")).thenReturn("yes");
		when(request.getParameter("health_care_contribution")).thenReturn("yes");
		servlet.doPost(request, response);
		
		verify(response).sendRedirect("/mandatoryContractForm.jsp");

	}

}
