package contracts;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.mockito.Mockito;

import contracts.servlets.DataServlet;

public class TestDataServlet extends Mockito{

	@Test
	public void servlet_should_not_go_further_if_radio_not_checked_and_null() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		DataServlet servlet = new DataServlet();
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		
		when(request.getParameter("contract")).thenReturn(null);
		
		servlet.doGet(request, response);
		
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_not_go_further_if_radio_not_checked_and_empty() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		DataServlet servlet = new DataServlet();
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		
		when(request.getParameter("contract")).thenReturn("");
		
		servlet.doGet(request, response);
		
		verify(response).sendRedirect("/");
	}
	
	@Test
	public void servlet_should_go_further_if_radio_is_checked_to_employment_contract() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		DataServlet servlet = new DataServlet();
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		
		when(request.getParameter("contract")).thenReturn("employment_contract");
		
		servlet.doGet(request, response);
		
		verify(response).sendRedirect("/employmentContractForm.jsp");
	}
	
	@Test
	public void servlet_should_go_further_if_radio_is_checked_to_contract_work() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		DataServlet servlet = new DataServlet();
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		
		when(request.getParameter("contract")).thenReturn("contract_work");
		
		servlet.doGet(request, response);
		
		verify(response).sendRedirect("/contractWorkForm.jsp");
	}
	
	@Test
	public void servlet_should_go_further_if_radio_is_checked_to_mandatory_contract() throws IOException, ServletException {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		DataServlet servlet = new DataServlet();
		PrintWriter writer = mock(PrintWriter.class);
		when(response.getWriter()).thenReturn(writer);
		
		when(request.getParameter("contract")).thenReturn("mandatory_contract");
		
		servlet.doGet(request, response);
		
		verify(response).sendRedirect("/mandatoryContractForm.jsp");
	}
}
